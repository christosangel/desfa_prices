# desfa_prices



A simple js script to monitor and record changes in desfa prices.

Data source :

[http://www.xn--mxafd0dp.gr/scada/balancing-hourly.php?la=EN](http://www.xn--mxafd0dp.gr/scada/balancing-hourly.php?la=EN)

## INSTALL INSTRUCTIONS (Windows)

Visit [https://nodejs.org/en/download/](https://nodejs.org/en/download/), download and install **node.js**.

Visit [https://gitlab.com/christosangel/desfa_prices](https://gitlab.com/christosangel/desfa_prices):

 - click the **download** button,
 - select **zip** and
 - download **desfa_prices-main.zip**
 - Extract **desfa_prices-main.zip** folder from zip file

---


![1.png](screenshots/1.png)

---







Open Command Prompt and type (or copy-paste) the following commands:

`cd Desktop\desfa_prices-main`

`npm install puppeteer`

`npm init -y`

---

 Now let's run  the script for the first time, by typing :

`node main.js`


 Give it  about 10 to 20 seconds, and a new window of chrome will open.

It will be refreshing every 10 seconds, while the current prices will be shown on the command prompt window.

---

![6.png](screenshots/6.png)

---

Everytime the prices change, you will be notified in the command prompt, and a  new line containing the new prices will be added in **prices.csv**.

The **prices.csv** file can be viewed by using an office application (Excel, LibreOffice Calc), using a text editor like Wordpad, or  by clicking on prices.html

Now you can close the Command prompt.

---


### CREATE SHORTCUT for main js


Right-click on main.js and select *Create Shortcut*.

Right-click on  **Shortcut of main\.js**, 
 * select *Properties*:


  On tab *Shortcut*:  

 - Click *Change Icon*,

 - select *Browse*, 

 - navigate to **desfa_prices-main** folder, 

 - select **desfa-logo.ico**, 

 - click *Open*.


 On tab *General*:

 -   *Add name* of your liking
 -  *Open with* : Node.js
 -  Click *OK*


 Paste shortcut to your desktop

That's it!

---
  
### PREFERENCES

You can run Chrome **Headless** (no browser window will appear, however the script is running in the background).

You can also adjust the interval of price updates to your liking.


To proceed to these changes, open **main.js** with Workpad and change the respective lines(line 10, line 56) following the simple instructions that you find there:
  
![4.png](screenshots/4.png)



![5.png](screenshots/5.png)



### CREATE A SHORTCUT for **prices.html**


Right-click on **prices_viewer.html** and select *Create Shortcut*.

Right-click on  **Shortcut of prices_viewer.html**
 

 * select *Properties*:


  On tab *Shortcut*:  

 - Click *Change Icon*,

 - select *Browse*, 

 - navigate to **prices_viewer-main** folder, 

 - select **prices_viewer.ico**

 - click *Open*.


 On tab *General*:

 -   *Add name* of your liking
 -  *Open with* : add the name of your prefered browser
 -  Click *OK*


That's it!

---

### RUN

Navigate to your desktop, after staring main.js, double-click on created shortcut of prices.html.

A new tab will open in your browser.



The **prices.csv** is presented in this tab:

---



![12.png](screenshots/12.png)


---

The page is refreshing every 5 seconds.

In order to change the frecuency of the pade refreshing, change the value at line  90 of main.js

---


![11.png](screenshots/11.png)


---

