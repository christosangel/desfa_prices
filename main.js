//╺┳┓┏━╸┏━┓┏━╸┏━┓   ┏━┓┏━┓╻┏━╸┏━╸┏━┓
// ┃┃┣╸ ┗━┓┣╸ ┣━┫   ┣━┛┣┳┛┃┃  ┣╸ ┗━┓
//╺┻┛┗━╸┗━┛╹  ╹ ╹╺━╸╹  ╹┗╸╹┗━╸┗━╸┗━┛
//a script by Christos Angelopoulos, 1636419555
const puppeteer = require('puppeteer');
//var http = require('http');
var fs = require('fs');
// /*old main
  (async () =>
  {
  // if you want to run headless, take out {headless:false} from the next line
    const browser = await puppeteer.launch();
    const page = await browser.newPage();
    await page.goto('http://www.xn--mxafd0dp.gr/scada/balancing-hourly.php?la=EN');
    function desfa (){
      (async () =>
      {
        await page.reload({ waitUntil: ["networkidle0", "domcontentloaded"] });
        const result = await page.evaluate(() =>
        {
          var allTableData = document.querySelector("#tbl_Data_id ");
          var totalNumbeOfRows = allTableData.rows.length;
          let prices1 = document.querySelector('.Global_tbl_cls tr:nth-child(' + totalNumbeOfRows + ') td:nth-child(1)');
          let prices2 = document.querySelector('.Global_tbl_cls tr:nth-child(' + totalNumbeOfRows + ') td:nth-child(2)');
          let prices3 = document.querySelector('.Global_tbl_cls tr:nth-child(' + totalNumbeOfRows + ') td:nth-child(3)');
          let prices4 = document.querySelector('.Global_tbl_cls tr:nth-child(' + totalNumbeOfRows + ') td:nth-child(4)');
          let today = new Date();
          let timeDirty = today.toTimeString();
          let timeClean = (timeDirty.slice(0, 8));
          return timeClean + ',' + prices1.innerText + "," + prices2.innerText + "," + prices3.innerText + "," + prices4.innerText + '\n'
        })
        //console.log(result);

        console.log('Desfa prices have been captured succesfully.');
        let today0 = new Date;
        let timeDirty0 = today0.toTimeString();
        let timeClean0 = (timeDirty0.slice(0, 17));
        console.log('┌───────────────┬───────────────────────────────────────┐');
        console.log('│Time of update │', timeClean0,'                    │');
        console.log('├───────────────┼───────────────────────────────────────┤');
        let newPrices = (result.slice(9, 46));
        //console.log(result.length);
        console.log('│New prices     │', newPrices, '│');
        console.log('├───────────────┼───────────────────────────────────────┤');
        //console.log(newPrices.length);
        fs.readFile('prices.csv', 'utf8', (err, data) =>{
          if(err){
            console.log(err);
            return;
          }
          let long = (data.length);
          //console.log(long);
          let line = long - 38
          //console.log(line);
          let lastLine = (data.slice(line, long - 1));
          console.log('│Previous prices│', lastLine, '│');
          console.log('└───────────────┴───────────────────────────────────────┘');
          //console.log(lastLine.length);
          if(newPrices === lastLine) {console.log('The prices remained unchanged. \n');}
          else {console.log('*** The prices just changed ***\n', 'Prices appended to prices.csv\n');
                  fs.appendFile('prices.csv', result, err => {
                    if(err){console.err;return}
                  });

          };
        });
    //read files , get data STRING
  fs.readFile('prices.csv', 'utf8', (err, data) =>{
  if(err){
    console.log(err);
    return;
  };
  const r = data.split("\n");

  let r00 = r[0];
  let m00 = r00.split(",");

  let row0 = `<tr><td>${m00[0]}</td><td>${m00[1]}</td><td>${m00[2]}</td><td>${m00[3]}</td><td>${m00[4]}</td></tr`;
  let day = " ";
  let rows = " ";
  for (var z = 0; z < r.length; z++)
  {
  if (r[z] !==""){
  let day = r[z].replace(/[0-9][0-9]:[0-9][0-9]:[0-9][0-9],/, "").replace(/,.*$/,"")
  let nextday = r[z + 1].replace(/[0-9][0-9]:[0-9][0-9]:[0-9][0-9],/, "").replace(/,.*$/,"")
  //console.log("day=", day);
  //console.log("nextday=", nextday);

  //let r0 =`<tr><td>${r[z].replace(/,/g,"</td><td>")}</td></tr>`;
  if (day === nextday || nextday == "") {
    let r0 =`<tr><td>${r[z].replace(/,/g,"</td><td>")}</td></tr>`;
    rows = rows + r0;
  }
  else {let r0 =`<tr class="bold"><td>${r[z].replace(/,/g,"</td><td>")}</td></tr>`;
  rows = rows + r0;};

  };

};

  let htmlContent = `<!DOCTYPE html><html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <!--In order to change the frecuency of the page refreshing, change the value at line below-->
    <meta http-equiv="refresh" content="10" >
    <link rel="stylesheet" href="style.css">
    <link rel="icon" href="desfa-logo.ico">
    <title>Desfa Prices</title>
  </head>
  <body>
  <div class="desfa">
  		     <div class="image">
		   				<img src="desfa-logo.ico" alt="">
		   			</div>

  <h2 class="h2"> DESFA Imbalance Settlement Prices </h2>
 
  </div>
     		     <div class="Heading2">
  <a href="2ViewFile.html">View File (static)</a>

    
		   			</div> 
  <p>Last update : ${timeClean0.slice(0, 8)} EEST</p>
      <div class="pinakes">
        <table class="epikefalida">
        ${row0}
        </table>
        <table class="times">
        ${rows}
        </table>
      </div>
  </body>
</html>`;


  fs.writeFile('1Prices.html', htmlContent, err => {
        if(err){console.err;return}
      });

});

})();
}
    //run every 10000 msec, you can change the interval to your preferences
    setInterval(desfa, 10000);
  })();

